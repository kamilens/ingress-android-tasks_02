# ingress-android-02-tasks

## Tasks

### Note: Read all the reading materials before start tasks

- [ ] Write the kotlin code snippet that shows Byte, Short, Int, Long, Float and Double min and max values
- [ ] Write the kotlin code snippet that assigns 1 to x if y is greater than 0
- [ ] Suppose that score is a variable of type double. Write the kotlin code snippet that increases the score by 5 marks
  if score is between 80 and 90
- [ ] Suppose that time is a variable of type int which shows current time. Write the kotlin code snippet that prints
  “Good Day” if time is between 6 - 18 else print “Good Night”
- [ ] Suppose that temperature is a variable of type int which shows current temperature. Write the kotlin code snippet
  that prints when temperature i
    - [ ] Between -40 and down Freezing ->
    - [ ] Between -40 - -20 Very Cold ->
    - [ ] Between -20 - 0 Cold ->
    - [ ] Between 0 - 20 Mild ->
    - [ ] Between 20 - 40 Warm ->
    - [ ] Between 40 and up Dangerous ->
- [ ] Write a program that takes Triple and print it
- [ ] Write a program that takes Pair and print it
- [ ] Write a program that takes the current year and checks if it is Leap year or not (Leap year means the largest year
  with 366 days).
